/*
	Lesson Proper
	What is Javascript?
		It is a scripting programming language that enables us to make interactive web pages.

	2 ways to add comments in javascript
		// or ctrl + / = creates a single line comments
		ctrl + shift + / = create a multi line comments

*/

console.log("Hello, World!");
/*
	console.log allow us to show/display data in our console.
*/
/* Mini Activity*/

console.log("Van Excel");
console.log("Fried Chicken");
/*
	Statement and Syntax 
	
	Statements are instructions/expression we add to our program which will be communicated to our computer.
			   Computer will then to interpret these instructions and perform the task. 

	Syntax is a set of rules that describes how statements are properly made/constructed
	
	ex.
	console.log()		   

*/

/*
	Variables
		-are containers of data
		-allow us to save data within our script or program
		-to create variables we use the keyword let
	Syntax:

	let variableName = "data"
*/	
let name = "Van Excel"

console.log(name);
// save numbers in variables:
let	num = 5;
let num2 = 10;

console.log(num);
console.log(num2)
//You could check/display the values of multiples variables

console.log(name,num,num2);
//we cannot display the value of a variale that is not yet created/declared

let myVariable;
// You can create variable without providing an initial value but will be assigned as undefine
console.log(myVariable);

/*
		Creating variables has 2 steps
		1. Declaration = using let or const
		2. Initialization  = providing initial value to our variable
*/

myVariable = "new value";
console.log(myVariable);

let	bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);
// You can update the value of a variable declared with the let keyword
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

//We cannot update with the let keyword
//We cannot create another variable with the same name

/*let	 bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);*/

/*
		Const

		const keyword allow us to ccreate variable like let but it cannot be updated.
		you cant create const without initialization
*/

const pi = 3.1416;
console.log(pi);

/*pi = 3.15;
console.log(pi);	*/


// declaring without initialization will cause error
/*const plateNum;
console.log(plateNum);
*/

/*Mini Activity*/

let	 name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";
const tin = "12333-1234";
console.log(name2,role,tin);
/*
	Conventions in creating variable/constant names;
	REMEMBER : LET VARIABLES CAN BE UPDATED WIHLE CONST VARIABLES CANNOT.
	Naming variables should use camelCase style. Examples (firstName . secondName)

	variable names should define their value
*/


/*

		DATA TYPES 

		strings = alpha numeric characters to create word, phrase , name, etc.
				= created with string literals likes single quotes and quotes



*/
let country = "Philippines";
let province = "Metro Manila";

console.log(province,country);
/*You can combine strings using + sign . This is called concatenation*/

let address = province + "," + country;
console.log(address);

let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines	";
let country2 = "U.S.A";
let country3 = "South Korea";
let country4 = "Japan";

let capital1 = city1 + ", " + country1;
let capital2 = city3 + ", " + country2;
let capital3 = city4 + ", " + country4;

console.log(capital1);	
console.log(capital2);	
console.log(capital3);	

/*
		DATA TYPES

		Integers = number data can be used for mathematical operations
		
*/

let	number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

let sum1 = number1 + number2;
console.log(sum1);

let sum2 = 16 + 4;
console.log(sum2);

let numString1 = "30";
let numString2 = "50";
/*Thsese are numeric string  , it will not add but just combine*/
let sumString1 = numString1	+ numString2;
console.log(sumString1);
//When an integer is added to a string, it will concatenate
let sum3 = number1	 + numString2;
console.log(sum3);
/*

		Data Types

		Boolean (true or false)
				is used for logical operations. and if-else conditions.


*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;


console.log("Is she married " + isMarried);
console.log("Is Curry an MVP? " + isMVP);
console.log("Is he the current admin? " + isAdmin);


/*

		Data Types

		Arrays  - are a special kind of data type. It is used to store multiple values
				- usually used to store multiple values of same data type
				- it can store different data type but its not a good practice
				- values in array are seperated in cooma
*/

let array1 = ["Goku", "Gohan", "Goten", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 5000];

console.log(array1);
console.log(array2);


/*
		OBJECTS are another special kind of data types that mimic real world items/objects.
		It is used to create complex data structure that contains pieces of information that relate to each other
		
		Each field in an object is called property,seperated by comma

		Object can actually group of different data

*/

let hero1 = {
	heroName : "One Punch Man",
	realName: "Saitama",
	income: 5000,
	isActive: true
};

/*Mini-Activity*/

let soad = ["Serj", "Daron", "Shavo", "Ontronik"];

let person = {
	firstName : "Van Excel",
	lastName : "Rondina",
	isDeveloper: true,
	age:24	
};

console.log(soad);
console.log(person);

/* Null and Undefined  */

//Null is explicit absensce of data/value. This is to show that a variable contains nothing.
//Undefined is variable is created but has no initial value yet.

/*sampple of null*/

let foundResult  = null;

/*sample of undefined*/

let sampleVariable;
/*-------------------------*/


let person2 = {
	name : "Peter",
	age	 :42
};
//Access or display the value pf an object we use the dot notation

console.log(person2.name);
console.log(person2.age);

//Code below will result undefine. The person2 object exist but no isAdmin in the property
console.log(person2.isAdmin);

/*
		FUNCTIONS 
			= functions are block of codes that tell our computer to perform
			  a task when called or invoke
			= it is reusable pieces of code

*/

console.log("Good Afternoon, Everyone! Welcome to my application!");

/*create function using function keyword*/
function greet(){

	console.log("Good Afternoon, Everyone! Welcome to my application!");
}

//function invocation 
greet();
greet();

/*Mini Activity*/

let favoriteFood = "Fried Chicken";
let valueSum = 150 + 9 ;
let valueProduct = 100 * 90 ;
let userActive = true;

let favoriteRestaurant = ["Honey Night", "Lami Keeyow", "Tea Barrel", "Chowking","Jollibee"];


let favoriteArtist = {

		firstName : "Ed ",
		lastName : "Sheeran",
		stageName: "Ed Sheeran",
		birthday: "February 17, 1991",
		age: "31",
		bestAlbum : "÷",
		bestSong : "perfect",
		bestTvShow: null,
		bestMovie : null,
		isActive : true,

};

console.log(favoriteFood);
console.log(valueSum);
console.log(valueProduct);
console.log(userActive);
console.log(favoriteRestaurant);
console.log(favoriteArtist);


//CONTINUATION OF  FUNCTIONS
	//Parameters and aArguments
	// 'name ' is called parameter
	// Parameter acts as a named variables/container that exist ONLY in the functions.This is used to store information to act as a stand-in 
	//

	function printName(name) {
		console.log (`My name is ${name}`)
	};
	//Data passed in the function : argument
	//Representation of the argument within the function : parameter
	printName("Van");


	function displayNum(number){
		console.log(number)
	};

	displayNum(300);
	displayNum(3001);


/*Mini Activiyu*/
function displayMessage(message){

	console.log(message)
}

displayMessage("Javascript is fun!");


// Multiple Parameters and Arguments
	//Function cannot only receive a single argument but also multiple arguments as long as it matches the number of parameters in the function

function displayFullName(firstName, lastName,age){
	console.log( `${firstName} ${lastName} is ${age}`)
};

displayFullName ( "Van", "Rondina", 24)


//return keyword
	// return keyword is used so that function may return a value
	//it also stops the process of the function after the return keyword

	function createFullName(firstName, middleName , lastName) {

		return `${firstName} ${middleName} ${lastName}`
		console.log ("I will no longer run because the function's value/result has been returned")
	};

	let fullname1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullname1);